CREATE TABLE PATIENT(
    PatientId INT PRIMARY KEY,
    Name VARCHAR(50),
    DOB Date,
    Address VARCHAR(50)
);

CREATE TABLE Prescription(
    PatientId INT,
    PrescriptionId INT PRIMARY KEY,
    Drug VARCHAR(50),
    Dates DATE,
    Dosage VARCHAR(50),
    Doctor VARCHAR(50),
    Secretary VARCHAR(50),
    FOREIGN KEY (PatientId) REFERENCES PATIENT(PatientId)
);