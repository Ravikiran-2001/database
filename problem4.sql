CREATE TABLE DOCTOR(
    DoctorId INT PRIMARY KEY,
    DoctorName VARCHAR(20),
    Secretary VARCHAR(20)
);
CREATE TABLE Patient(
    DoctorId INT,
    PatientId INT PRIMARY KEY,
    PatientDOB Date,
    PatientAddress VARCHAR(50),
    FOREIGN KEY (DoctorId) REFERENCES Doctor(DoctorId) 
);
CREATE TABLE Prescription(
    PatientId INT,
    PrescriptionId INT PRIMARY KEY,
    Drug VARCHAR(50),
    Dates DATE,
    Dosage VARCHAR(50),
    FOREIGN KEY (PatientId) REFERENCES Patient(PatientId)
);