CREATE TABLE CLIENT(
    ClientId INT PRIMARY KEY,
    Name VARCHAR(20),
    Location VARCHAR(50)
);
CREATE TABLE Manager(
    ClientId INT,
    ManagerId INT PRIMARY KEY,
    Manager_name VARCHAR(50),
    Manager_location VARCHAR(50),
    FOREIGN KEY  (ClientId) REFERENCES CLIENT(ClientId)
);
CREATE TABLE Contract(
    ManagerId INT,
    ContractId INT PRIMARY KEY,
    Estimated_cost INT(500),
    Completion_date DATE,
    FOREIGN KEY  (ManagerId) REFERENCES Manager(ManagerId)
);
CREATE TABLE Staff(
    ContractId INT,
    StaffId INT PRIMARY KEY,
    Staff_name VARCHAR(50),
    Staff_location VARCHAR(50),
    FOREIGN KEY  (ContractId) REFERENCES Contract(ContractId)
);